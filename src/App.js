import React, { Component } from 'react'
import { View } from 'react-native'
import { NativeRouter, Route } from 'react-router-native'

import MainScreen from './components/MainScreen'

export default class App extends Component {
  render () {
    return (
      <NativeRouter>
        <View>
          <Route exact path='/' render={() => <MainScreen />} />
          {/* <Route exact path='/' render={() => <MainScreen />} />
          <Route exact path='/' render={() => <MainScreen />} /> */}
        </View>
      </NativeRouter>
    )
  }
}
